
#include <iostream>
#include <array>
#include <map>
#include <string>
#include <fstream> 
#include <typeinfo>

void generateCrc8Table( std::array< uint8_t , 0x100 > & _pCrcTable, const uint8_t &  _polynom );

int main()
{
	std::array < uint8_t , 0x100 > CRC8_Dallas;
	std::array < uint8_t, 0x100 > CRC8_ETSI_EN_302_307;
	std::map<std::string, std::array < uint8_t, 0x100 > > CRC8_Value;
	std::map< std::string, uint8_t > CRC_Samples = { { "Dallas_Maxim"  , 0x31} ,{ "ETSI_EN_302_301",0xD5 } };

	generateCrc8Table(CRC8_Dallas, CRC_Samples["Dallas_Maxim"]);
	generateCrc8Table(CRC8_ETSI_EN_302_307, CRC_Samples["ETSI_EN_302_301"]);

	CRC8_Value.insert( { { "Dallas_Maxim"  , CRC8_Dallas} , { "ETSI_EN_302_301" , CRC8_ETSI_EN_302_307  } } ); 

	std::ofstream outputFile;
	
	outputFile.open("text.txt", std::ofstream::out);

//add sausage  
	//and pizza

	//some cola

	//delete cola and pizza
	
	//asdasdasdlaksdlkasjdlaksdlkadslaksdlkjasdlkasdlkasdladsaldksadlaksdlaksdaldskjasdlkajsd;la
	//ssda
	
	outputFile.close();
    return 0;
}


void generateCrc8Table(std::array< uint8_t, 0x100 > & _pCrcTable, const uint8_t &  _polynom)
{	
	uint8_t l_crcValue;
	for (int i = 0; i < _pCrcTable.size(); i++) 
	{
		l_crcValue = i;
		for (int j = 0; j < 8; j++) 
		{
			l_crcValue = (l_crcValue & (1 << 7)) ? ((l_crcValue << 1) ^ (_polynom)) : (l_crcValue << 1);
		}
		_pCrcTable[i] = l_crcValue;
	}
}

